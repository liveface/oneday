package entitty;

public class News {
	private String newImgUrl;
	private String title;
	private String content;
	public News() {
		super();
	}
	public News(String newImgUrl, String title, String content) {
		super();
		this.newImgUrl = newImgUrl;
		this.title = title;
		this.content = content;
	}
	public String getNewImgUrl() {
		return newImgUrl;
	}
	public void setNewImgUrl(String newImgUrl) {
		this.newImgUrl = newImgUrl;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString() {
		return "News [newImgUrl=" + newImgUrl + ", title=" + title
				+ ", content=" + content + "]";
	}
	
}
