package com.flydev.oneday.activity;


import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.WindowManager;

import com.flydev.oneday.R;
import com.flydev.oneday.fragment.BaseDialogFragment;


public class WelcomeActivity extends BaseActivity {
	
	Handler handler = new Handler(){
		@Override
		public void handleMessage(Message msg) {
			Intent intent = new Intent(WelcomeActivity.this,HomeActivity.class);
			startActivity(intent);
		}
		
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rLayout.setBackgroundResource(R.drawable.welcome);
        rLayout.setClipToPadding(true);
        rLayout.setFitsSystemWindows(true);
		handler.sendEmptyMessageDelayed(1, 2000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 透明状态栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // 透明导航栏
            getWindow().addFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
		finish();
	}
}
