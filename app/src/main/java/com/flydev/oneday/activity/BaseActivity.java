package com.flydev.oneday.activity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;
/**
 * BaseActivtity
 * Notitle
 * @author boshao
 *
 */
public abstract class BaseActivity extends FragmentActivity{	
	//满屏的绝对布局
	public RelativeLayout rLayout;
	//layout打气筒
	public LayoutInflater inflater;
    Context mContext;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		//requestWindowFeature(Window.FEATURE_NO_TITLE);
		inflater = LayoutInflater.from(this);	
		rLayout = new RelativeLayout(this);
		LayoutParams params = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 
				ViewGroup.LayoutParams.MATCH_PARENT);
		rLayout.setLayoutParams(params);
		setContentView(rLayout);
        mContext = this;
	}
	
	/**
	 * Activity的finish()方法
	 */
	public void finish(){
		super.finish();
	}

    public boolean isNetwork() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return false;
        }
        if (connectivityManager.getActiveNetworkInfo() == null) {
            return false;
        }
        return connectivityManager.getActiveNetworkInfo().isAvailable();
    }


    private static AsyncHttpClient httpClient = new AsyncHttpClient();

    public void Post(final Context mContext, String url,
                            RequestParams params, final Handler handler, final int handleCode) {
        httpClient.post(mContext, url, params, new TextHttpResponseHandler() {

            @Override
            public void onSuccess(int arg0, Header[] arg1, String arg2) {
                loge(arg2);
                Message msg = new Message();
                msg.obj = arg2;
                msg.what = handleCode;
                handler.sendMessage(msg);
            }

            @Override
            public void onFailure(int arg0, Header[] arg1, String arg2,
                                  Throwable arg3) {
                Toast.makeText(mContext,
                        "Code:" + arg0 + " " + arg2 + "  数据请求失败", 1).show();
            }
        });
    }
    protected void loge(String s){
        Log.e("------", s);
    }
}
