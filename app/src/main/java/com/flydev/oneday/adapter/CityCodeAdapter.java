package com.flydev.oneday.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

/**
 * Created by Modificator on 2014/12/14.
 */
public class CityCodeAdapter extends CursorAdapter {
    private LayoutInflater mInflater;
    private Context mContext;


    public CityCodeAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c);
        mContext = context;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return mInflater.inflate(android.R.layout.simple_list_item_1, parent, false);

        //return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
//        ((TextView) view).setText(cursor.getString(cursor.getColumnIndex("province")) + "-" +
//                cursor.getString(cursor.getColumnIndex("city")) + "-" +
//                cursor.getString(cursor.getColumnIndex("area")));
        ((TextView) view).setTextColor(0xffffffff);
        ((TextView) view).setText(cursor.getString(cursor.getColumnIndex("area")) + "-" + cursor.getString(cursor.getColumnIndex("province")));

    }


}
