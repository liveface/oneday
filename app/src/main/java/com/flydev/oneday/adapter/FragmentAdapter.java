package com.flydev.oneday.adapter;

import java.util.ArrayList;

import com.flydev.oneday.fragment.AlarmFragment;
import com.flydev.oneday.fragment.NewsFragment;
import com.flydev.oneday.fragment.WeatherFragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class FragmentAdapter extends FragmentStatePagerAdapter{
	FragmentManager fm;
	ArrayList<Fragment> fList;
	
	public FragmentAdapter(FragmentManager fm) {
		super(fm);
		fList = new ArrayList<Fragment>();
		fList.add(new WeatherFragment());
		fList.add(new NewsFragment());
		fList.add(new AlarmFragment());
	}

	@Override
	public Fragment getItem(int position) {
		return fList.get(position);
	}

	@Override
	public int getCount() {
		return fList.size();
	}

}
