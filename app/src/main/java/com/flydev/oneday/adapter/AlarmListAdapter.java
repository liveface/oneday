package com.flydev.oneday.adapter;

import java.util.ArrayList;
import java.util.List;

import com.flydev.oneday.R;

import entitty.AlarmModel;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Switch;

public class AlarmListAdapter extends BaseAdapter{
	private List<AlarmModel> alarmList;
	private Context context;
	
	public AlarmListAdapter(List<AlarmModel> alarmList, Context context) {
		super();
		this.alarmList = alarmList;
		this.context = context;
		this.alarmList = new ArrayList<AlarmModel>();
		init();
	}
	
	/**
	 * 初始化试图
	 */
	private void init() {
		AlarmModel alarmModel = new AlarmModel();
		alarmList.add(alarmModel);
		AlarmModel alarmModel2 = new AlarmModel();
		alarmList.add(alarmModel2);
	}

	@Override
	public int getCount() {
		return alarmList.size();
	}

	@Override
	public Object getItem(int position) {
		return alarmList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View container, ViewGroup parent) {
		View v;

		if(container == null){
			v = LayoutInflater.from(context).inflate(R.layout.alarm_listview,parent,false);
		}else{
			v = container;
		}
		
		
		
		return v;
	}

}
