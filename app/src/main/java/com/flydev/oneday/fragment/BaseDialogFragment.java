package com.flydev.oneday.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flydev.oneday.R;

/**
 * 整个app的BaseDailog
 * 
 * @author boshao
 * 
 */
public class BaseDialogFragment extends DialogFragment {
	public static final String TAG = "BaseDialogFragment";
	static View baseView=null;

	public static BaseDialogFragment newInstance(View view) {
		BaseDialogFragment f = new BaseDialogFragment();
        baseView = view;
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		int style = DialogFragment.STYLE_NORMAL;
		style = DialogFragment.STYLE_NO_TITLE;
		setStyle(style, R.style.baseDailogTheme);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
        if (baseView==null){
            dismiss();return null;}
		//baseView = inflater.inflate(R.layout.base_fragment_dialog, container,
		//		false);
		return baseView;
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		Log.d("TAG", TAG + "onCancel");
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		Log.d("TAG", TAG + "onDestroyView");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.d("TAG", TAG + "onDestroy");
	}

	@Override
	public void onDetach() {
		super.onDetach();
		Log.d("TAG", TAG + "onDetach");
	}

}
