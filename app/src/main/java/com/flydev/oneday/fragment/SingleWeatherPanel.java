package com.flydev.oneday.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.flydev.oneday.R;
import com.flydev.oneday.bean.Weather;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Modificator on 2015/2/14.
 */
@SuppressLint("ValidFragment")
public class SingleWeatherPanel extends Fragment {

    String cityID;
    String cityName;
    View rootView;

    public SingleWeatherPanel(String cityID, String cityName) {
        this.cityID = cityID;
        this.cityName = cityName;
    }


    public void initWeather() {
        getWeather();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.pager_of_weather, container, false);
        initWeather();
        return rootView;
    }


    static AsyncHttpClient httpClient = new AsyncHttpClient();

    private void getWeather() {
        httpClient.get("http://api.liebao.cn/newtab/index.php?r=weathernew/getXiaomiData&city_id=" + cityID, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.e("------", responseString.substring(14, responseString.length() - 1));
                refreshWeather(responseString.substring(14, responseString.length() - 1));
            }
        });
    }

    private void refreshWeather(String jsonObject) {
        Log.e("------", (rootView == null) + "");
        Weather weather = JSON.parseObject(jsonObject, Weather.class);
        ((TextView) rootView.findViewById(R.id.panel_weather_tv_address)).setText(weather.getWeatherInfo().getCity());
        ((TextView) rootView.findViewById(R.id.panel_weather_tv_temperature)).setText(weather.getSk().getWeatherinfo().getTemp() + "°");
        ((TextView) rootView.findViewById(R.id.panel_weather_tv_week)).setText(getWeek());
        ((TextView) rootView.findViewById(R.id.panel_weather_tv_date)).setText(getDate());
        ((ImageView) rootView.findViewById(R.id.img_weather)).setImageResource(getWeatherRes(weather.getSk().getWeatherinfo().getWeather()));
    }


    private String getWeek() {
        Date dt = new Date();
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(dt);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0)
            w = 0;
        return weekDays[w];
    }

    private String getDate() {
        Date date = new Date();
        SimpleDateFormat dateFm = new SimpleDateFormat("yyyy.MM.dd");
        return dateFm.format(date);
    }

    private int getWeatherRes(String w) {
        HashMap<String, Integer> weatherRec = new HashMap<>();
        weatherRec.put("晴", R.drawable.ic_weather_sunshine);
        weatherRec.put("多云", R.drawable.ic_weather_cloudy2sunny);
        weatherRec.put("阴", R.drawable.ic_weather_overcast);
        weatherRec.put("雾", R.drawable.ic_weather_fog);
        weatherRec.put("特大暴雨", R.drawable.ic_weather_downpour);
        weatherRec.put("大暴雨", R.drawable.ic_weather_downpour);
        weatherRec.put("暴雨", R.drawable.ic_weather_downpour);
        weatherRec.put("雷阵雨", R.drawable.ic_weather_thunder_shower);
        weatherRec.put("阵雨", R.drawable.ic_weather_light_rain);
        weatherRec.put("中雨", R.drawable.ic_weather_light_rain);
        weatherRec.put("小雨", R.drawable.ic_weather_light_rain);
        weatherRec.put("雨夹雪", R.drawable.ic_weather_light_rain);
        weatherRec.put("暴雪", R.drawable.ic_weather_heavy_snow);
        weatherRec.put("阵雪", R.drawable.ic_weather_heavy_snow);
        weatherRec.put("大雪", R.drawable.ic_weather_heavy_snow);
        weatherRec.put("中雪", R.drawable.ic_weather_heavy_snow);
        weatherRec.put("小雪", R.drawable.ic_weather_slight_snow);
        weatherRec.put("强沙尘暴", R.drawable.ic_weather_sandstorm);
        weatherRec.put("沙尘暴", R.drawable.ic_weather_sandstorm);
        weatherRec.put("沙尘", R.drawable.ic_weather_sandstorm);
        weatherRec.put("扬沙", R.drawable.ic_weather_sandstorm);
        weatherRec.put("冰雹", R.drawable.ic_weather_hail);
        weatherRec.put("浮尘", R.drawable.ic_weather_sandstorm);
        weatherRec.put("霾", R.drawable.ic_weather_fog);
        return weatherRec.get(w);
    }

    public String getCityName() {
        return cityName;
    }

    public String getCityID() {
        return cityID;
    }
}
