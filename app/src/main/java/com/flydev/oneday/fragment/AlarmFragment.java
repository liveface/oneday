package com.flydev.oneday.fragment;

import java.util.List;


import com.flydev.oneday.R;
import com.flydev.oneday.adapter.AlarmListAdapter;
import com.flydev.oneday.utils.DialogUtils;

import entitty.AlarmModel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Switch;

public class AlarmFragment extends Fragment implements OnClickListener{
	View view;
	ListView alarmLv;
	ImageButton addAlarmIb,delAlarmIb;
	List<AlarmModel> alarmList;
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.alarm, container,false);
		initView();
		return view;
	}
	private void initView() {
		alarmLv = (ListView) view.findViewById(R.id.listview_alarm);
		alarmLv.setAdapter(new AlarmListAdapter(alarmList,view.getContext()));
		
		addAlarmIb = (ImageButton) view.findViewById(R.id.btn_add_alarm);
		delAlarmIb = (ImageButton) view.findViewById(R.id.btn_del_alarm);
		addAlarmIb.setOnClickListener(this);
		delAlarmIb.setOnClickListener(this);
	}
	
	//点击监听
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_add_alarm:
			DialogUtils.showDialog(this.view,null);
			break;
		case R.id.btn_del_alarm:
			DialogUtils.showDialog(this.view,null);
			break;
		}
	}
}
