package com.flydev.oneday.fragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.flydev.oneday.R;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class NewsFragment extends Fragment {
    View v, webback;
    ListView newsListView;
    WebView webView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.news, container, false);
        initView();
        return v;
    }

    /**
     * 初始化控件
     *
     * @param v
     */
    private void initView() {
        newsListView = (ListView) v.findViewById(R.id.listview_new);
        webback = v.findViewById(R.id.web_back);
        SimpleAdapter adapter = new SimpleAdapter(getActivity(), getList(), R.layout.news_listview,
                new String[]{"title"}, new int[]{R.id.tv_news_title});
        //webback.setVisibility(View.GONE);
        newsListView.setAdapter(adapter);
        {
            webView = (WebView) v.findViewById(R.id.webView);
            final WebSettings settings = webView.getSettings();
            settings.setSupportZoom(true);

            //WebView启用Javascript脚本执行
            settings.setJavaScriptEnabled(true);
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            settings.setAppCacheEnabled(true);
            settings.setCacheMode(WebSettings.LOAD_DEFAULT);
            settings.setAllowFileAccess(true);
            settings.setDomStorageEnabled(true);
            webView.setWebChromeClient(new WebChromeClient());

            webView.addJavascriptInterface(new JsOperation(), "one");
            webView.setWebViewClient(new MyWebViewClient());
            webView.loadUrl("http://www.flydev.cc/");
            //webback.setVisibility(webView.canGoBack() ? View.VISIBLE : View.GONE);
        }
        webback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (webView.canGoBack()) {
                    webView.goBack();
                    //webback.setVisibility(webView.canGoBack() ? View.VISIBLE : View.GONE);
                }
            }
        });
    }

    private List<Map<String, String>> getList() {
        List<Map<String, String>> mList = new ArrayList<Map<String, String>>();
        Map<String, String> map = new HashMap<String, String>();
        map.put("title", "News");
        Map<String, String> map2 = new HashMap<String, String>();
        map.put("title", "News2");
        mList.add(map);
        mList.add(map2);
        return mList;
    }

    final class JsOperation {
        JsOperation() {
        }
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            //webback.setVisibility(webView.canGoBack() ? View.VISIBLE : View.GONE);
            return false;
        }
    }
}
