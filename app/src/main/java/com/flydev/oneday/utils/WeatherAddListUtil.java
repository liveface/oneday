package com.flydev.oneday.utils;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.flydev.oneday.App;
import com.flydev.oneday.R;
import com.flydev.oneday.activity.BaseActivity;
import com.flydev.oneday.activity.HomeActivity;
import com.flydev.oneday.adapter.CityCodeAdapter;
import com.flydev.oneday.adapter.WeatherAdapter;
import com.flydev.oneday.fragment.SingleWeatherPanel;

import java.util.AbstractMap;
import java.util.List;

/**
 * Created by Modificator on 2014/12/14.
 */
public class WeatherAddListUtil {

    View v = null;
    AssetsDatabaseManager databaseManager;
    Cursor cursor = null;
    SQLiteDatabase database;
    CityCodeAdapter adapter = null;
    List<SingleWeatherPanel> fragments;
    WeatherAdapter weatherAdapter;
    DialogFragment dialogFragment;

    public WeatherAddListUtil(View v, List<SingleWeatherPanel> fragments, WeatherAdapter weatherAdapter, DialogFragment dialogFragment) {
        this.fragments = fragments;
        this.weatherAdapter = weatherAdapter;
        this.v = v;
        this.dialogFragment = dialogFragment;
        init();
    }

    private void init() {
        databaseManager = AssetsDatabaseManager.getManager();
        database = databaseManager.getDatabase("cityid.db");
        cursor = database.rawQuery("select _id,province,city,area,weatherid from idmap where area like ? or city like ? or province like ?", new String[]{"%", "%", "%"});

        EditText et_cityInput = (EditText) v.findViewById(R.id.panel_weather_city_input);
        ListView lv_citySelect = (ListView) v.findViewById(R.id.panel_weather_city_select);
        adapter = new CityCodeAdapter(v.getContext(), cursor, false);
        lv_citySelect.setAdapter(adapter);
        lv_citySelect.setOnItemClickListener(itemClickListener);
        et_cityInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String searchCode = (s.toString().isEmpty() ? "" : s.toString()) + "%";
                cursor = database.rawQuery("select _id,province,city,area,weatherid from idmap where area like ? union all select _id,province,city,area,weatherid from idmap where province like ?", new String[]{searchCode, searchCode});
                adapter.changeCursor(cursor);
                adapter.notifyDataSetInvalidated();
            }
        });
    }

    AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            cursor.moveToFirst();
            cursor.move(position);
            String cityName = cursor.getString(3), cityNum = cursor.getString(4);

            if (App.weather_Inf.add(new AbstractMap.SimpleEntry<String, String>(cityName, cityNum))) {
                fragments.add(new SingleWeatherPanel(cityNum, cityName));
                {
                    SQLiteDatabase database = App.dbOpenHelper.getWritableDatabase();
                    database.execSQL("insert into weather_inf(city_name,city_num) values(?,?)", new String[]{cityName, cityNum});
                    database.close();
                }
                weatherAdapter.notifyDataSetChanged();
                dialogFragment.dismiss();
            } else {
                Toast.makeText(dialogFragment.getActivity(), "该城市已添加", Toast.LENGTH_SHORT).show();
            }
        }
    };
}
